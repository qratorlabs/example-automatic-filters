local shell = {}

function shell.err(args)
	ngx.log(ngx.ERR, args)
	ngx.exit(ngx.ERROR)
end

function shell.output(args)
	local ngx_pipe = require("ngx.pipe")

	local proc, err = ngx_pipe.spawn(args)
	if not proc then shell.err('spawn: ', err) end

	local ok, err = proc:shutdown('stdin')

	local ok, err = proc:stdout_read_line()
	if not ok then shell.err('stdout_read_line: ', err) end
	assert( ok == "ok" )

	local d, err = proc:stdout_read_all()
	if not d then shell.err('stdout_read_all: ', err) end

	return d
end

return shell
